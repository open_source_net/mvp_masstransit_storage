﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using SharedContracts;
using System.Threading.Tasks;

namespace MassTransitBus.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StorageController : ControllerBase
    {
        private readonly IPublishEndpoint _publishEndpoint;
        public StorageController(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        [HttpPost]
        public async Task<IActionResult> Create(StorageItem item)
        {
            await _publishEndpoint.Publish<StorageItemCreate>(new
            {
                Id = item.Id,
                Bucket = item.Bucket,
                MimeType = item.MimeType,
                RawData = item.RawData
            });
            return Ok();
        }
    }
}
