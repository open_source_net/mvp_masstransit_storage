﻿using MassTransit;
using MassTransitBus.Model;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace MassTransitBus
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            await Host.CreateDefaultBuilder(args)
               .ConfigureServices(services =>
               {
                   services.AddMassTransit(x =>
                   {
                       x.SetKebabCaseEndpointNameFormatter();
                       x.UsingRabbitMq((context, cfg) =>
                       {
                           cfg.Host(MassTransitBusSettingsSingleton.Instance.RabbitMQHost, "/", h =>
                           {
                               h.Username(MassTransitBusSettingsSingleton.Instance.RabbitMQUser);
                               h.Password(MassTransitBusSettingsSingleton.Instance.RabbitMQPass);
                           });

                           cfg.ConfigureEndpoints(context);
                       });
                   });
               })
               .ConfigureWebHostDefaults(webBuilder =>
               {
                   webBuilder
                   .UseUrls(MassTransitBusSettingsSingleton.Instance.BusHost)
                   .UseStartup<Startup>()
                   .ConfigureLogging(logging =>
                   {
                       //replace with using logger
                       logging.ClearProviders();
                   });
               })
               .Build()
               .RunAsync();
        }
    }
}
