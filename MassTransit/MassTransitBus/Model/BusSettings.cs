﻿using Singletons.Templates;

namespace MassTransitBus.Model
{
    public class BusSettings
    {
        public string StorePath { get; set; } = "bus_settings.json";
        public string BusHost { get { return SettingsData.Data.BusHost; } }
        public string RabbitMQHost { get { return SettingsData.Data.Host; } }
        public string RabbitMQUser { get { return SettingsData.Data.UserName; } }
        public string RabbitMQPass { get { return SettingsData.Data.UserPass; } }
        public JsonSettings<SettingsData> SettingsData { get; set; }
        private BusSettings()
        {
            SettingsData = new JsonSettings<SettingsData>(StorePath);
        }
    }
}
