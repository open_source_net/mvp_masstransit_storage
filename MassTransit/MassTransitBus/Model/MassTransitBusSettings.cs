﻿using Singletons;

namespace MassTransitBus.Model
{
    public class MassTransitBusSettingsSingleton
    {
        private MassTransitBusSettingsSingleton()
        {
        }

        public static BusSettings Instance
        {
            get
            {
                return Singleton<BusSettings>.Instance;
            }
        }

        public static BusSettings ResetValues
        {
            get
            {
                return Singleton<BusSettings>.ReloadInstance;
            }
        }
    }
}
