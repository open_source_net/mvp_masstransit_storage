﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MassTransitBusClient
{
    public class BaseClient
    {
        private const string HTTP = "http";

        protected string _area = "/";
        protected string _appName;
        protected string _host;
        protected int _timeout = 30;

        public BaseClient(string host, string appName, string area = "api/", int timeout = 30)
        {
            _appName = appName;
            _area = area;
            _host = (host.StartsWith(HTTP) ? string.Empty : $"{HTTP}://") + host;
            _timeout = timeout;
        }

        public HttpClient CreateClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(new Uri(_host), $"{_area}{_appName}");
            client.Timeout = TimeSpan.FromSeconds(_timeout);
            return client;
        }

        public HttpClient CreateJsonHeaderClient()
        {
            var client = new HttpClient() { Timeout = TimeSpan.FromSeconds(_timeout) };
            client.BaseAddress = new Uri(new Uri(_host), $"{_area}{_appName}");
            client.DefaultRequestHeaders
                  .Accept
                  .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
