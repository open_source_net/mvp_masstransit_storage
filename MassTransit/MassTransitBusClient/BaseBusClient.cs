﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace MassTransitBusClient
{
    public class BaseBusClient<T> : BaseClient where T : class
    {
        public BaseBusClient(string host, string endpoint="live", string area = "health/")
            : base(host, endpoint, area)
        {
        }

        public async Task<IList<T>> RequestAll()
        {
            HttpResponseMessage response;
            IList<T> result = new List<T>();
            try
            {
                using (var client = CreateJsonHeaderClient())
                {
                    response = await client.GetAsync(client.BaseAddress);
                }
                result =await response.Content.ReadFromJsonAsync<List<T>>();
            }
            catch (Exception ex)
            {
                //todo logging
            }
            return result;
        }

        public  async Task<T> Request(string paramStr)
        {
            HttpResponseMessage response;
            T result = Activator.CreateInstance<T>();

            try
            {
                using (var client = CreateJsonHeaderClient())
                {
                    response = await client.GetAsync($"{client.BaseAddress}{(paramStr.StartsWith("?")?string.Empty:"?")}{paramStr}");
                }
                result = await response.Content.ReadFromJsonAsync<T>();
            }
            catch (Exception ex)
            {
                //todo logging
            }
            return result;
        }

        public async Task<string> Post(T item)
        {
            HttpResponseMessage response;
            string result = string.Empty;
            try
            {
                using (var client = CreateJsonHeaderClient())
                {
                    response = await client.PostAsJsonAsync(client.BaseAddress, item);
                }
                result = await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                //todo logging
            }
            return result;
        }
    }
}
