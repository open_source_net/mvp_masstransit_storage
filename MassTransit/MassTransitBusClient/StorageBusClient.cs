﻿using SharedContracts;

namespace MassTransitBusClient
{
    public class StorageBusClient : BaseBusClient<StorageItem>
    {
        public StorageBusClient(string host, string endpoint = "storage", string area = "api/")
           : base(host, endpoint, area)
        {
        }
    }
}
