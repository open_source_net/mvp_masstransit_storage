﻿using System;

namespace SharedContracts
{
    public class StorageItem
    {
        public string Id { get; set; }
        public string Bucket { get; set; }
        public string MimeType { get; set; }
        public byte[] RawData { get; set; } 
    }
}
