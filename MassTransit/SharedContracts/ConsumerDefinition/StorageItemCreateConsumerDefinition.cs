﻿using MassTransit;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace SharedContracts.Consumer
{
   
    public class StorageItemCreateConsumerDefinition : ConsumerDefinition<StorageItemCreateConsumer>
    {
        public static string Endpoint { get; private set; } = "storage-item-created-event";
        public StorageItemCreateConsumerDefinition()
        {
            EndpointName = Endpoint;
            ConcurrentMessageLimit = 8;
        }

        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator,
            IConsumerConfigurator<StorageItemCreateConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseMessageRetry(r => r.Intervals(100, 200, 500, 800, 1000));
            endpointConfigurator.UseInMemoryOutbox();
        }
    }
}
