﻿using MassTransit;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace SharedContracts.Consumer
{
    public class StorageItemCreateConsumer : IConsumer<StorageItemCreate>
    {
        public virtual async Task Consume(ConsumeContext<StorageItemCreate> context)
        {
            Console.WriteLine(JsonConvert.SerializeObject(context.Message));
        }
    }
}
