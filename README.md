# MVP_Minio_MassTransit

## Name

MVP for MassTransit (over RabbitMQ) and MinioStorage usage 

## Description

R&D for usage MinioStroage with MassTransit bus (Use Case Includes)
This project demonstrates possibility to usage internal storages (only Minio in current release) with Service Buses (like MassTransit in current release).

Only research purpose. All services provides 'as-is' with no warranty.
Don't usage in production mode! No logging, unit-tests, auth tokens, etc.
 
## Structure:

**MassTransit** 
- _MassTransitBus_ - Implementation MassTransit as Simple .Net Core API.
- _MassTransitBusClient_ - Clients for bus (only POST requests handles on server-side in current release).
- _SharedContracts_ - Bus Contract Models.

**Minio**
- _MinioStorage_ - Minio Client wrapper with common-usage functions (will be inherit from Template StorageWrapper).
- _MinioUploadService_ - Bus Event Consumer.

**UseCases**
- _MediaApi_ - .Net Core Api demonstrates access to minio storage data with GET requests.
- _MediaUploader_ - .Net Console App demostrates access to Minio storage with Upload data via MassTransit.
    
**Utils**
- Contains Extensions, Helpers and Templates libraries. 

## License
Licensed under the GNU General Public License (GPL) Version 2.

## Project status
Under development
