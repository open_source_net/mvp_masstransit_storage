﻿using System;
using Newtonsoft.Json;
using System.Timers;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Collections.Generic;
using PuppeteerSharp;
using System.IO;
using System.Threading.Tasks;
using Force.Crc32;
using MediaApi.Model;

public class Screenshoter
{
    private const string TMP_DIR = "screenshots";
    private int _updateInterval;
  
    private IDictionary<string, string> _pagesFilePaths;
    private IDictionary<string, uint> _screenshotCRCs;


    private readonly Thread _monitorThread;

    public Screenshoter(IEnumerable<string> pagesList, int updateIntervalInMins=1440)
    {
        if (!Directory.Exists(TMP_DIR))
            Directory.CreateDirectory(TMP_DIR);

        _updateInterval = updateIntervalInMins;
        _pagesFilePaths = pagesList
                            .Distinct()
                            .ToDictionary(m=>m, m=> $"{TMP_DIR}/{System.Web.HttpUtility.UrlEncode(m)}.png");
        _screenshotCRCs = _pagesFilePaths.ToDictionary(m => Path.GetFileName(m.Value), m=> uint.MaxValue);
        _monitorThread = new Thread(InitTimer);
    }

    public async Task StartAsync()
    {
        await DoScreenshots();
        await UploadScreenshots();
        await Task.Run(() =>_monitorThread.Start());
    }

    private void InitTimer()
    {
        var timer = new System.Timers.Timer();
        timer.Elapsed += OnTimedEvent;
        timer.Enabled = true;
        timer.Interval = _updateInterval * 60 * 1000;
        timer.AutoReset = true;
    }

    private async void OnTimedEvent(object source, ElapsedEventArgs e)
    {
        await DoScreenshots();
        await UploadScreenshots();
    }

    private async Task DoScreenshots()
    {
        using (var browserFetcher = new BrowserFetcher())
        {
            await browserFetcher.DownloadAsync();
            await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = true });
            await using var page = await browser.NewPageAsync();
            await page.SetViewportAsync(new ViewPortOptions
            {
                Width = 1240,
                Height = 2000
            });

            foreach (var pageUrl in _pagesFilePaths.Keys)
            {
                try
                {
                    await page.GoToAsync(pageUrl, WaitUntilNavigation.Networkidle2);
                    var htmlContent = await page.GetContentAsync();
                    File.WriteAllText(_pagesFilePaths[pageUrl].Replace(".png",".html"), htmlContent);
                    await page.ScreenshotAsync(_pagesFilePaths[pageUrl], new ScreenshotOptions() { FullPage = true });
                }
                catch (Exception ex)
                {
                    //todo process if unavailable pageUrl or file write error
                }
            }
        }
    }
    private async Task UploadScreenshots()
    {
        foreach (var item in Directory.GetFiles(TMP_DIR))
        {
            var arr = File.ReadAllBytes(item);
            var crc32 = Crc32CAlgorithm.Compute(arr);
            if (_screenshotCRCs.TryGetValue(Path.GetFileName(item), out var existCRC) && existCRC != crc32)
            {
               await  UploaderServiceSingleton.Instance.PostByteArray(crc32.ToString(), Path.GetExtension(item).Substring(1), arr);
            }
        }
    }
}
