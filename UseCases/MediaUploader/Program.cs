﻿using MediaApi.Model;
using System;
using System.IO;

var defaultList = UploaderServiceSingleton.Instance.UrlsSource;
if (File.Exists(defaultList))
{
    Screenshoter monitor = new Screenshoter(File.ReadAllLines(defaultList));
    await monitor.StartAsync();
    Console.ReadKey();
}
else
{
    File.WriteAllText(defaultList, "https://netex24.net/");
    throw new Exception($"Please edit {defaultList} in root with websites urls for monitoring");
}