﻿namespace MediaApi.Model
{
    public class SettingsData
    {
        public string BusEndpoint { get; set; } = "http://*:5017";
        public string UrlsSource { get; set; } = "urls.txt";
        public string Bucket { get; set; } = "screenshots";

        private SettingsData()
        {
        }
    }
}
