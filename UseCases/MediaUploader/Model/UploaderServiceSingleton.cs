﻿using Singletons;

namespace MediaApi.Model
{
    public class UploaderServiceSingleton
    {
        private UploaderServiceSingleton()
        {
        }

        public static UploaderService Instance
        {
            get
            {
                return Singleton<UploaderService>.Instance;
            }
        }

        public static UploaderService ResetValues
        {
            get
            {
                return Singleton<UploaderService>.ReloadInstance;
            }
        }
    }
}
