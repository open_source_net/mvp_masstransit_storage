﻿using MassTransitBusClient;
using Singletons.Templates;
using System.Threading.Tasks;

namespace MediaApi.Model
{
    public class UploaderService
    {
        private StorageBusClient _busClient; 

        public string StorePath { get; set; } = "uploader_settings.json";
        public string BusEndpoint { get { return SettingsData.Data.BusEndpoint; } }
        public string UrlsSource { get { return SettingsData.Data.UrlsSource; } }
        public JsonSettings<SettingsData> SettingsData { get; set; }

        private UploaderService()
        {
            SettingsData = new JsonSettings<SettingsData>(StorePath);
            if (string.IsNullOrEmpty(BusEndpoint))
                throw new System.Exception("define endpoint for MassTransit Bus endpoint");
            _busClient = new StorageBusClient(BusEndpoint);
        }

        public  async Task PostByteArray(string id, string fileExtension, byte[] arr)
        {
            await _busClient.Post(new SharedContracts.StorageItem()
            {
                Bucket = SettingsData.Data.Bucket,
                Id = id,
                MimeType = $"image/{fileExtension}",
                RawData = arr
            });
        }
    }
}
