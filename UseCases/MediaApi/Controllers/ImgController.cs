﻿using MediaApi.Model;
using Microsoft.AspNetCore.Mvc;
using MinioStorage.Interface;
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace MediaApi.Controllers
{
    /// <summary>
    /// Image Provider for Minio storage
    /// </summary>
    [ApiController]
    public class ImgController : ControllerBase
    {
        private string _coldStorage = MediaApiSettingsSingleton.Instance.ColdStorage; 
        private string _defaultImg = MediaApiSettingsSingleton.Instance.DefaultImg;
       
        private IMinioClientWrapper _minio;
        
        /// <summary>
        ///  Default constructor
        /// </summary>
        /// <param name="minIo">DI</param>
        public ImgController(IMinioClientWrapper minIo)
        {
            _minio = minIo;
        }

        /// <summary>
        /// Get Image
        /// </summary>
        /// <param name="bucket">minio bucket</param>
        /// <param name="imageId">image name</param>
        /// <param name="ext">image file extension</param>
        /// <returns></returns>
        [HttpGet]
        [Route("/{bucket}/{imageId}.{ext}")]
        [Route("/images/{imageId}")]
        [Route("/images/{imageId}.{ext}")]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = 16800, VaryByQueryKeys = new string[] { "bucket", "imageId" })]
        public async Task<IActionResult> Get(string bucket="default", string imageId = "", string ext="jpeg")
        {
            Stream stream = null;
            var prefix = new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
            var path = Path.Combine(prefix, "wwwroot", "img", imageId);

            if (string.IsNullOrEmpty(imageId) || imageId == "_thumbnail")
            {
                stream = new FileStream(Path.Combine(prefix, "wwwroot", "img", _defaultImg), FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            else if (System.IO.File.Exists(path))
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            else
            {
                var hasImage = false;
                try
                {
                    hasImage = await _minio.DownloadFile(bucket, imageId, path);
                }
                catch (Exception ex)
                {
                    //todo log
                }
                if (!hasImage)
                {
                    using (WebClient wc = new WebClient())
                    {
                        var data = await wc.DownloadDataTaskAsync($"https://{_coldStorage}/{(bucket == "default" ? "images" : bucket)}/{imageId}.{ext}");
                        System.IO.File.WriteAllBytes(path, data);
                        
                        await _minio.UploadBytesToBacket(bucket, imageId, $"image/{ext}", data);
                    }
                }
                stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            // no needs stream dispose, because  it will disposed in FileResulExecutorBase
            return File(stream, $"image/{ext}");
        }
    }
}