﻿using Helpers;
using MediaApi.Model;
using Microsoft.AspNetCore.Mvc;
using MinioStorage.Interface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace MediaApi.Controllers
{
    /// <summary>
    /// Image Provider for Minio storage with lazy resize mode
    /// </summary>
    [ApiController]
    public class ImgSizedController : ControllerBase
    {

        private string _coldStorage = MediaApiSettingsSingleton.Instance.ColdStorage; 
        private string _defaultImg = MediaApiSettingsSingleton.Instance.DefaultImg;
     
        private IMinioClientWrapper _minio;
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="minIo">DI</param>
        public ImgSizedController(IMinioClientWrapper minIo)
        {
            _minio = minIo;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageId"></param>
        /// <param name="size"></param>
        /// <param name="ext"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/{bucket}/{imageId}.{ext}")]
        [Route("/mimages/{imageId}")]
        [Route("/mimages/{imageId}.{ext}")]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration =16800, VaryByQueryKeys = new string[] { "bucket", "size", "imageId" })]
        public async Task<IActionResult> Get(string bucket= "default", string imageId = "", int size = 1280, string ext="jpeg")
        {
            if (size < 150)
                size = 150;
            else if (size < 350)
                size = 350;
            else if (size < 650)
                size = 650;
            else
                size = 1280;

            Stream stream=null;

            var prefix = new Uri(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase)).LocalPath;
            var filePath = Path.Combine(prefix, "wwwroot","img",$"{size}",$"{imageId}");
            if (string.IsNullOrEmpty(imageId))
            {
                stream = new FileStream(Path.Combine(prefix, "wwwroot", "img", _defaultImg), FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            else if (System.IO.File.Exists(filePath))
            {
                stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            else
            {
                var path = Path.Combine(prefix, "wwwroot", "img", imageId);
                var hasImage = false;
                try
                {
                    hasImage = await _minio.DownloadFile(bucket, imageId, path);
                }
                catch
                { }
                if (!hasImage)
                {
                    using (WebClient wc = new WebClient())
                    {
                        var data = await wc.DownloadDataTaskAsync($"https://{_coldStorage}/images/{imageId}.{ext}");
                        System.IO.File.WriteAllBytes(path, data);
                        await _minio.UploadBytesToBacket(bucket, imageId, $"image/{ext}", data);

                    }
                }
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);


                string base64String = string.Empty;
                byte[] imageBytes = new byte[0];
                Stream resizeStream = null;
                ImageResizer imgResizer = new ImageResizer(size, 1280);
                try
                {
                    using (Image image = imgResizer.GetWebP(stream))
                    {
                        using (MemoryStream m = new MemoryStream())
                        {
                            image.Save(m, ImageFormat.Bmp);
                            resizeStream = imgResizer.BlobResize(m, true);
                            imageBytes = (resizeStream as MemoryStream).ToArray();
                        }
                    }
                }
                catch(Exception ex)
                {
                    //todo log
                }
                if (resizeStream == null)
                {
                    try
                    {
                        using (Image image = Image.FromStream(stream))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                resizeStream = imgResizer.BlobResize(m, false);
                                imageBytes = (resizeStream as MemoryStream).ToArray();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //todo log
                    }
                }
                if (imageBytes.Length > 0)
                {
                    try
                    {
                        var dir = Path.GetDirectoryName(filePath);

                        if (!Directory.Exists(dir))
                            Directory.CreateDirectory(dir);

                        System.IO.File.WriteAllBytes(filePath, imageBytes);
                    }
                    catch (Exception ex)
                    {
                        //todo log
                    }
                }
            }
            // no needs stream dispose, because  it will disposed in FileResulExecutorBase
            return File(stream, $"image/{ext}");
        }
    }
}