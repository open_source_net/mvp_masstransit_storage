﻿using Singletons.Templates;

namespace MediaApi.Model
{
    public class MediaApiSettings
    {
        public string StorePath { get; set; } = "api_settings.json";
        public string Host { get { return SettingsData.Data.Host; } }
        public string ColdStorage { get { return SettingsData.Data.ColdStorage; } }
        public string DefaultImg { get { return SettingsData.Data.ColdStorage; } }
        public JsonSettings<SettingsData> SettingsData { get; set; }

        private MediaApiSettings()
        {
            SettingsData = new JsonSettings<SettingsData>(StorePath);
        }
    }
}
