﻿using Singletons;

namespace MediaApi.Model
{
    public class MediaApiSettingsSingleton
    {
        private MediaApiSettingsSingleton()
        {
        }

        public static MediaApiSettings Instance
        {
            get
            {
                return Singleton<MediaApiSettings>.Instance;
            }
        }

        public static MediaApiSettings ResetValues
        {
            get
            {
                return Singleton<MediaApiSettings>.ReloadInstance;
            }
        }
    }
}
