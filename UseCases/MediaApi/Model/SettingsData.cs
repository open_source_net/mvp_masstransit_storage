﻿namespace MediaApi.Model
{
    public class SettingsData
    {
        public string Host { get; set; } = "http://*:5013";
        public string ColdStorage { get; set; }
        public string DefaultImg { get; set; }
        private SettingsData()
        {
        }
    }
}
