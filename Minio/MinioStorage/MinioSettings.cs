﻿using MinioStorage.Interface;
using System;
using System.IO;

namespace MinioStorage
{
    //default MinioStorage credentials
    public class MinioSettings : IMinioSettings
    {
        public string Endpoint { get; set; } = "localhost:9000";
        public string AccessKey { get; set; } = "minioadmin";
        public string SecretKey { get; set; } = "minioadmin";
        public bool Secure { get; set; }
        public MinioSettings()
        { 
        }

        public void Load(string settings)
        {
            if (File.Exists(settings))
            {
                var settingsFromFile = Newtonsoft.Json.JsonConvert.DeserializeObject<MinioSettings>(File.ReadAllText(settings));
                Endpoint = settingsFromFile.Endpoint;
                AccessKey = settingsFromFile.AccessKey;
                SecretKey = settingsFromFile.SecretKey;
                Secure = settingsFromFile.Secure;
            }
            else
            {
                File.WriteAllText(settings, Newtonsoft.Json.JsonConvert.SerializeObject(new MinioSettings()));
                throw new Exception($"fill settings file {settings} and restart app");
            }
        }
    }
}
