﻿using Helpers;
using Minio;
using MinioStorage.Interface;
using System;
using System.IO;
using System.Threading.Tasks;

namespace MinioStorage
{
    //todo inherit from StorageWrapper<T,V>:
    //                  where T : class, IStorageClient
    //                  where V : class, IStorageSettings
    public class MinioClientWrapper: IMinioClientWrapper, IDisposable
    {
        const string pathToSettings = "minio_credentials.json";

        private string _bucketName;

        //todo update with IStorageSettings
        private IMinioSettings _settings;
        //todo update with  IStorageClient
        private MinioClient _client;

        public Stream CurStreamContent { get; set; }

        public MinioClientWrapper()
        {
            InitClient(new MinioSettings());
        }
       
        public void InitClient(IMinioSettings settings, string defaultBucket = "default")
        {
            _bucketName = defaultBucket;
            _settings = settings;
            _settings.Load(pathToSettings);
            _client = new MinioClient();
            _client
                .WithEndpoint(settings.Endpoint)
                .WithCredentials(settings.AccessKey, _settings.SecretKey)
                .WithSSL(settings.Secure)
                .Build();
        }

        public async Task<bool> DownloadFile(string bucketName, string fileName, string path)
        {
            CurStreamContent = new MemoryStream();
            var getObjectArgs = new GetObjectArgs()
                .WithBucket(bucketName)
                .WithObject(fileName)
                .WithFile(path)
                .WithCallbackStream((stream) =>
                 {
                     stream.CopyTo(CurStreamContent);
                 });

            var file = await _client.GetObjectAsync(getObjectArgs);
            return file != null;
        }

        public async Task<bool> CheckExist(string bucketName, string fileName)
        {
            if (CurStreamContent != null)
            {
                CurStreamContent.Close();
                CurStreamContent.Dispose();
            }
            CurStreamContent = new MemoryStream();
            var getObjectArgs = new GetObjectArgs()
                .WithBucket(bucketName)
                .WithObject(fileName)
                .WithCallbackStream((stream) =>
                {
                    stream.CopyTo(CurStreamContent);
                });

            var file = await _client.GetObjectAsync(getObjectArgs);
            return file != null;
        }

        public async Task UploadBytesToBacket(string bucketName, string fileName, string contentType,  byte[] byteArr)
        {
            using (MemoryStream ms = new MemoryStream(byteArr))
            {
                try
                {
                    await UploadStreamToBacket(bucketName, fileName, contentType, ms);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        
        public async Task UploadStreamToBacket(string contentType, Stream stream)
        {
            await UploadStreamToBacket(_bucketName, contentType, stream);
        }
        public async Task UploadStreamToBacket(string bucketName, string contentType, Stream stream)
        {
            var blobId = TransliterationHelper.Front(Guid.NewGuid().ToString());
            await UploadStreamToBacket(bucketName, blobId, contentType, stream);
        }

        public async Task UploadStreamToBacket(string bucketName, string fileName, string contentType, Stream stream)
        {
            if (stream.Length != 0)
            {
                var beArgs = new BucketExistsArgs()
                       .WithBucket(bucketName);
                bool found = await _client.BucketExistsAsync(beArgs);
                if (!found)
                {
                    var mbArgs = new MakeBucketArgs()
                        .WithBucket(bucketName);
                    await _client.MakeBucketAsync(mbArgs);
                }

                var putObjectArgs = new PutObjectArgs()
                    .WithBucket(bucketName)
                    .WithStreamData(stream)
                    .WithObject(fileName)
                    .WithObjectSize(stream.Length)
                    .WithContentType(contentType);

                await _client.PutObjectAsync(putObjectArgs);
            }
            else
               throw new Exception("zero length");
        }

        public async Task UploadFileToBacket(string bucketName, string fileName, string contentType, string fileTo)
        {
            if (File.Exists(fileTo))
            {
                var beArgs = new BucketExistsArgs()
                       .WithBucket(bucketName);
                bool found = await _client.BucketExistsAsync(beArgs);
                if (!found)
                {
                    var mbArgs = new MakeBucketArgs()
                        .WithBucket(bucketName);
                    await _client.MakeBucketAsync(mbArgs);
                }

                var putObjectArgs = new PutObjectArgs()
                    .WithBucket(bucketName)
                    .WithObject(fileName)
                    .WithFileName(fileTo)
                    .WithContentType(contentType);

                await _client.PutObjectAsync(putObjectArgs);
            }
            else
                throw new Exception("file not exist");
        }

        public void Dispose()
        {
            _client.Dispose();
            CurStreamContent.Close();
            CurStreamContent.Dispose();
        }
    }
}
