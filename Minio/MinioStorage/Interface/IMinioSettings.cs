﻿namespace MinioStorage.Interface
{
    public interface IMinioSettings
    {
        string Endpoint { get; set; }
        string AccessKey { get; set; }
        string SecretKey { get; set; }
        bool Secure { get; set; }

        void Load(string settings);
    }
}
