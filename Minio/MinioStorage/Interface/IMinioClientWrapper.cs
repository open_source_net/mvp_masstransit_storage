﻿using System.IO;
using System.Threading.Tasks;

namespace MinioStorage.Interface
{
    //todo try to common storage interface define
    public interface IMinioClientWrapper
    {
        Stream CurStreamContent { get; set; }

        void InitClient(IMinioSettings settings, string defaultBucket);
        
        Task<bool> CheckExist(string bucketName, string fileName);
        Task<bool> DownloadFile(string bucketName, string fileName, string path);

        Task UploadBytesToBacket(string bucketName, string fileName, string contentType,  byte[] stream);

        Task UploadStreamToBacket(string contentType, Stream stream);
        Task UploadStreamToBacket(string bucketName, string contentType, Stream stream);
        Task UploadStreamToBacket(string bucketName, string fileName, string contentType, Stream stream);

        Task UploadFileToBacket(string bucketName, string fileName, string contentType, string fileTo);
    }
}
