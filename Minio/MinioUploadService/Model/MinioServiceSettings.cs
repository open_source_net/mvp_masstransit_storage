﻿using MinioStorage;
using MinioStorage.Interface;
using Singletons.Templates;

namespace MinioUploadService.Model
{
    public class MinioServiceSettings
    {
        public string StorePath { get; set; } = "service_settings.json";
        public string RabbitMQHost { get { return SettingsData.Data.Host; } }
        public string RabbitMQUser { get { return SettingsData.Data.UserName; } }
        public string RabbitMQPass { get { return SettingsData.Data.UserPass; } }
        public JsonSettings<SettingsData> SettingsData { get; set; }
        public IMinioClientWrapper MinioClient { get; set; } = new MinioClientWrapper();
        private MinioServiceSettings()
        {
            SettingsData = new JsonSettings<SettingsData>(StorePath);
        }
    }
}
