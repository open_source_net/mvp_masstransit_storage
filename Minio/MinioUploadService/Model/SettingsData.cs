﻿namespace MinioUploadService.Model
{
    //Init with Default RabbitMQ Credentials
    public class SettingsData
    {
        public string Host { get; set; } = "localhost";
        public string UserName { get; set; } = "guest";
        public string UserPass { get; set; } = "guest";

        private SettingsData()
        {
        }
    }
}
