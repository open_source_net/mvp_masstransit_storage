﻿using Singletons;

namespace MinioUploadService.Model
{
    public class MinioServiceSingleton
    {
        private MinioServiceSingleton()
        {
        }

        public static MinioServiceSettings Instance
        {
            get
            {
                return Singleton<MinioServiceSettings>.Instance;
            }
        }

        public static MinioServiceSettings ResetValues
        {
            get
            {
                return Singleton<MinioServiceSettings>.ReloadInstance;
            }
        }
    }
}
