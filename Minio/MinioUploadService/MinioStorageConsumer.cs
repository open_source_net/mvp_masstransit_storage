﻿using MassTransit;
using MinioUploadService.Model;
using SharedContracts;
using SharedContracts.Consumer;
using System;
using System.Threading.Tasks;

namespace MinioUploadService
{
    public class MinioStorageConsumer: StorageItemCreateConsumer
    {
        public MinioStorageConsumer()
        { 
        }

        public override async Task Consume(ConsumeContext<StorageItemCreate> context)
        {
            await MinioServiceSingleton.Instance.MinioClient.UploadBytesToBacket(context.Message.Bucket, context.Message.Id, context.Message.MimeType, context.Message.RawData);
            Console.WriteLine($"store {context.Message.Id}:{context.Message.RawData.Length}");
        }
    }
}
