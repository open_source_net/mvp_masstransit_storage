﻿using MassTransit;
using MinioUploadService;
using MinioUploadService.Model;
using SharedContracts.Consumer;
using System;
using System.Threading;
using System.Threading.Tasks;

var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
{
    cfg.Host(MinioServiceSingleton.Instance.RabbitMQHost, h =>
    {
        h.Username(MinioServiceSingleton.Instance.RabbitMQUser);
        h.Password(MinioServiceSingleton.Instance.RabbitMQPass);
        h.ConfigureBatchPublish(x =>
        {
            x.Enabled = true;
            x.Timeout = TimeSpan.FromMilliseconds(2);
        });
    });
    cfg.ReceiveEndpoint(StorageItemCreateConsumerDefinition.Endpoint, e =>
    {
        e.Consumer<MinioStorageConsumer>();
    });

});

await busControl.StartAsync(new CancellationToken());

try
{
    Console.WriteLine("Press enter to exit");
    await Task.Run(() => Console.ReadLine());
}
finally
{
    await busControl.StopAsync();
}