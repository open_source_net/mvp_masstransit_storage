﻿using Singletons.Templates;

namespace Singletons.Interface
{
  
    public interface ISiteSettings<T> where T : class
    {
        public JsonSettings<T> Settings { get; set; }
    }
}
