﻿using Singletons.Interface;

namespace Singletons
{
    public abstract class SiteConfigWrapper<T,K> 
        where T : class,  ISiteSettings<K>
        where K : class
    {
        public static T Instance
        {
            get
            {
                return Singleton<T>.Instance;
            }
        }
        public static K Settings
        {
            get
            {
                return Singleton<T>.Instance.Settings.Data;
            }
        }
        public static T ResetValues
        {
            get
            {
                return Singleton<T>.ReloadInstance;
            }
        }
    }
}
