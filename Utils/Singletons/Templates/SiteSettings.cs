﻿using Singletons.Interface;

namespace Singletons.Templates
{

    public abstract class SiteSettings<T> : ISiteSettings<T> where T : class
    {
        public JsonSettings<T> Settings { get; set; }

        public SiteSettings()
        {
            Settings = new JsonSettings<T>( $"/{typeof(T).ToString().Replace(".","_").ToLowerInvariant()}_settings.json");
        }
    }

   
}
