﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.IO;
using System.Reflection;

namespace Singletons.Templates
{
    public class JsonSettings<T> where T : class
    {
        private object _lockObj = new object();
        protected string _filePath = string.Empty;
        private IMemoryCache _memoryCache;
        public T Data { get; set; }

        public JsonSettings(string filePath)
        {
            _filePath = System.AppDomain.CurrentDomain.BaseDirectory  + filePath;
            Load();
        }

        public void RegisterCache(IMemoryCache cache)
        {
            _memoryCache = cache;
        }
        public void Save()
        {
            lock (_lockObj)
            {
                File.WriteAllText(_filePath, Newtonsoft.Json.JsonConvert.SerializeObject(Data));
                if (_memoryCache != null)
                    _memoryCache.Remove(typeof(T).ToString());
                Load();
            }
        }

        public void Save(T fileData)
        {
            lock (_lockObj)
            {
                File.WriteAllText(_filePath, Newtonsoft.Json.JsonConvert.SerializeObject(fileData));
                if (_memoryCache != null)
                    _memoryCache.Remove(typeof(T).ToString());
                Load();
            }
        }

        public void Load()
        {
            if (File.Exists(_filePath))
            {
                try
                {
                    Data = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(File.ReadAllText(_filePath));
                }
                catch
                {
                }
                if (Data == null)
                {
                    File.Delete(_filePath);
                }
            }
            if(!File.Exists(_filePath))
            {
                try
                {
                    Data = Activator.CreateInstance<T>();
                }
                catch
                {
                }

                if (Data == null)
                {
                    ConstructorInfo cInfo = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[0], new ParameterModifier[0]);
                    Data = (T)cInfo.Invoke(null);
                }
                File.WriteAllText(_filePath, Newtonsoft.Json.JsonConvert.SerializeObject(Data));
                throw new Exception($"{_filePath}. Settings was create. Edit it or restart now.");
            }
        }

        public void ReloadSettings()
        {
            Load();
            if(_memoryCache !=null)
                _memoryCache.Set<T>(typeof(T).ToString(), Data, new DateTimeOffset(DateTime.Now.AddDays(1)));
        }

        public void LoadSettings()
        {
            if (_memoryCache != null)
                _memoryCache.GetOrCreate<T>(typeof(T).ToString(), entryItem =>
                {
                    entryItem.SetAbsoluteExpiration(new DateTimeOffset(DateTime.Now.AddDays(1))); Load(); return Data;
                });
            else
                Load();
        }

    }
}
