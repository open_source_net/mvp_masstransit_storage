﻿using Extensions;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.IO;

namespace Singletons.Templates
{
    public class XmlSettings<T> where T : class
    {
        public bool ExistFile { get; set; }
        protected string _xmlFilePath = string.Empty;
        protected double _cacheExpiredInHours;
        protected IMemoryCache _memCache;

        public T XMLFile { get; set; }
        public XmlSettings(string xmlFilePath, IMemoryCache memoryCache, double casheExpiredHours = 24)
        {
            _cacheExpiredInHours =  casheExpiredHours;
            _xmlFilePath  = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory,xmlFilePath);
            Load();
        }

        public void Save(T xmlFile)
        {
            ObjectXMLSerializer<T>.Save(xmlFile, _xmlFilePath);
            _memCache.Remove(typeof(T).ToString());
            Load();
        }

        public string XmlToString(T xmlFile)
        {
            return ObjectXMLSerializer<T>.XmlToString(xmlFile);
        }

        public T Load()
        {
            ExistFile = File.Exists(_xmlFilePath);
            if (!string.IsNullOrEmpty(_xmlFilePath)) {
                if (ExistFile)
                {
                    XMLFile = ObjectXMLSerializer<T>.Load(_xmlFilePath);
                }
                else
                {
                    XMLFile = Activator.CreateInstance<T>();
                    ObjectXMLSerializer<T>.Save(XMLFile, _xmlFilePath);
                }
            }
            return XMLFile;
        }

        public void Load(string path)
        {
            _xmlFilePath = path;
            Load();
        }

        public void LoadFromXML(string content)
        {
            XMLFile = ObjectXMLSerializer<T>.LoadFromXml(content);
        }

        public void ReloadSettings()
        {
            var key = typeof(T).ToString() + _xmlFilePath;
            XMLFile = _memCache.Update<T>(key, SettingsLock, () => Load(), DateTime.Now.AddHours(_cacheExpiredInHours));
        }

        public void LoadSettings()
        {
            var key = typeof(T).ToString() + _xmlFilePath;
            XMLFile = _memCache.Get<T>(key, SettingsLock, () => Load(), DateTime.Now.AddHours(_cacheExpiredInHours));
        }

        public static object SettingsLock = new object();
    }
}
