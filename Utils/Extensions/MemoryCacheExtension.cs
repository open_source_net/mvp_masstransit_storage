﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections;

namespace Extensions
{
    public static class MemoryCacheExtension
    {
        public static T Get<T>(this IMemoryCache cache, string key, object @lock, Func<T> selector, DateTime absoluteExpiration)
        {
            object value;
            if ((value = cache.Get(key)) == null
                || (value is IList && (value as IList).Count == 0))
            {
                lock (@lock)
                {
                    if ((value = cache.Get(key)) == null)
                    {
                        value = selector();
                        cache.Set(key, value, absoluteExpiration);
                    }
                }
            }
            return (T)value;
        }

        public static T Get<T>(this IMemoryCache cache, string key, object @lock, Action selector, DateTime absoluteExpiration)
        {
            object value;
            if ((value = cache.Get(key)) == null 
                || (value is IList && (value as IList).Count == 0))
            {
                lock (@lock)
                {
                    if ((value = cache.Get(key)) == null)
                    {
                        value = selector;
                        cache.Set(key, value, absoluteExpiration);
                    }
                }
            }
            return (T)value;
        }

        public static T Update<T>(this IMemoryCache cache, string key, object @lock, Func<T> selector, DateTime absoluteExpiration)
        {
            object value;
            if ((value = cache.Get(key)) == null 
                || (value is IList && (value as IList).Count == 0))
            {
                lock (@lock)
                {
                    value = selector();
                    cache.Set(key, value, absoluteExpiration);
                }
            }
            return (T)value;
        }

        public static T Update<T>(this IMemoryCache cache, string key, object @lock, Action selector, DateTime absoluteExpiration)
        {
            object value;
            if ((value = cache.Get(key)) == null 
                || (value is IList && (value as IList).Count == 0))
            {
                lock (@lock)
                {
                    value = selector;
                    cache.Set(key, value, absoluteExpiration);
                }
            }
            return (T)value;
        }
    }
}
